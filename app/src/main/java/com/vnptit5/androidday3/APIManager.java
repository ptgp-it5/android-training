package com.vnptit5.androidday3;

import com.vnptit5.androidday3.model.Item;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIManager {
    String SERVER = "https://api-demo-anhth.herokuapp.com";
    @GET("data.json") Call<Item> getItemData();
}
